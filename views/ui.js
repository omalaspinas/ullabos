let fdx0 = $("#fdx0");
let fdx1 = $("#fdx1");
let fdy0 = $("#fdy0");
let fdy1 = $("#fdy1");
let fdz0 = $("#fdz0");
let fdz1 = $("#fdz1");

let dx0 = $("#dx0");
let dx1 = $("#dx1");
let dy0 = $("#dy0");
let dy1 = $("#dy1");
let dz0 = $("#dz0");
let dz1 = $("#dz1");


// full domain ------------------------
fdx0.change(function(){
  dx0.attr({"min": fdx0.val()});
  dx1.attr({"min": dx0.val()});
  fdx1.attr({"min:": fdx0.val()});

  if(parseInt(fdx0.val()) > parseInt(dx0.val())){
    dx0.val(fdx0.val());
  }
})

fdx1.change(function(){
  dx0.attr({"max": dx1.val()});
  dx1.attr({"max": fdx1.val()});
fdx0.attr({"max:": fdx1.val()});

  if(parseInt(fdx1.val()) < parseInt(dx1.val())){
    dx1.val(fdx1.val());
  }
})

fdy0.change(function(){
  dy0.attr({"min": fdy0.val()});
  dy1.attr({"min": dy0.val()});
fdy1.attr({"min:": fdy0.val()});

  if(parseInt(fdy0.val()) > parseInt(dy0.val())){
    dy0.val(fdy0.val());
  }
})

fdy1.change(function(){
  dy0.attr({"max": dy1.val()});
  dy1.attr({"max": fdy1.val()});
fdy0.attr({"max:": fdy1.val()});

  if(parseInt(fdy1.val()) < parseInt(dy1.val())){
    dy1.val(fdy1.val());
  }
})

fdz0.change(function(){
  dz0.attr({"min": fdz0.val()});
  dz1.attr({"min": dz0.val()});
fdz1.attr({"min:": fdz0.val()});

  if(parseInt(fdz0.val()) > parseInt(dz0.val())){
    dz0.val(fdz0.val());
  }
})

fdz1.change(function(){
  dz0.attr({"max": dz1.val()});
  dz1.attr({"max": fdz1.val()});
fdz0.attr({"max:": fdz1.val()});

  if(parseInt(fdz1.val()) < parseInt(dz1.val())){
    dz1.val(fdz1.val());
  }
})


// domains -----------------------------
dx0.change(function(){
  dx1.attr({"min": dx0.val()});
});

dx1.change(function(){
  dx0.attr({"max": dx1.val()});
})

dy0.change(function(){
  dy1.attr({"min": dy0.val()});
});

dy1.change(function(){
  dy0.attr({"max": dy1.val()});
})

dz0.change(function(){
  dz1.attr({"min": dz0.val()});
});

dz1.change(function(){
  dz0.attr({"max": dz1.val()});
})
