"use strict"
var express = require("express")
var bodyParser = require('body-parser')
var request = require('request')
var http = require('http')

var app = express()

var router = require('./ressources/router')(app)
var config = require('./config/config.json')


app.set('view engine', 'ejs');

app.use(bodyParser.json() );  // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({// to support URL-encoded bodies
  extended: true
}));
app.use(express.static("node_modules"));
app.use(express.static(__dirname + "/views"));



var server = app.listen(config.server.port, function () {
	console.log("SERVER: has started on port 8080");
});

var socket = require('./ressources/socket')(server)
