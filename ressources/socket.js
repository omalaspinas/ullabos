var socket = require('socket.io')

var config = require('../config/config.json')
var xml = require('../class/xml.js')

module.exports = function(server){
  var io = socket(server)

  io.on("connection", function(socket){
    console.log('SOCKET: A connection has been made')
    console.log('SOCKET: The socket is', socket.id)


    socket.on("generate_xml", function(data){

      xml.generate_xml(data, function(){
        socket.emit("xml_generated")
      })

    })

    socket.on("download_file", function(data){
      xml.download_file(data, function(file){
        socket.emit("file_to_download", file);
      })
    })

  })
}
