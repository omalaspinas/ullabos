"use strict"

module.exports = function (app) {

  app.get('/', function (req, res) {
    res.render("index.ejs");
  });

  app.get('/test', function (req, res) {
    res.render("test.ejs");
  });

  app.get('/download_vti',function(req,res){
    console.log("download request received");
    res.download('tmp/gridDensity.vti','gridDensity.vti');
  })

};
